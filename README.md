# Copy Ignore Tool

- copies a directory, ignoring certain directories and/or files

## Usage

- copyignore -target=./target_directory -ignoreFile=config_text_file
    - ignoreFile defaults to .copyignore
    - target is required

# Publish

- dotnet publish -r win-x64 -c Release -p:PublishAot=true