﻿using FluentFTP;
using System.Net;

namespace CopyIgnore.FTP;

public class CopyIgnoreFTPClient : IDisposable
{
    private readonly FtpClient ftpClient;

    public CopyIgnoreFTPInfo? Info { get; private set; }

    public bool IsConnected => ftpClient.IsConnected;

    public FtpOperatingSystem ServerOS => ftpClient.ServerOS;

    internal CopyIgnoreFTPClient()
    {
        ftpClient = new FtpClient();
    }

    public CopyIgnoreFTPClient(CopyIgnoreFTPInfo info) : this()
    {
        Info = info;
    }

    private void Connect(CopyIgnoreFTPInfo info)
    {
        if (ftpClient.IsConnected)
        {
            ftpClient.Disconnect();
        }

        ftpClient.Credentials = new NetworkCredential
        {
            UserName = info.Username,
            Password = info.Password,
        };
        ftpClient.Port = info.Port;
        if (!ftpClient.IsConnected)
        {
            ftpClient.Connect();
        }
    }

    public void Disconnect()
    {
        if (ftpClient.IsConnected)
        {
            ftpClient.Disconnect();
        }
    }

    public void UpdateInfo(CopyIgnoreFTPInfo info)
    {
        Info ??= new CopyIgnoreFTPInfo();
        Info.Host = ftpClient.Host = info.Host;
        Info.Port = info.Port;
        Info.Username = info.Username;
        Info.Password = info.Password;
    }

    public void UploadFolder(string localFolder, string remoteWorkingFolder, Action<FtpProgress>? callback = null)
    {
        ArgumentNullException.ThrowIfNull(Info);
        Connect(Info);
        string directory = Path.GetFileName(localFolder);
        string? remoteTargetDirectory = ftpClient.GetNameListing().FirstOrDefault(x => x == $"{remoteWorkingFolder}{CopyIgnoreFTPConstants.unixPathSeparator}{directory}");

        ftpClient.SetWorkingDirectory(remoteWorkingFolder);
        if (remoteTargetDirectory is null)
        {
            ftpClient.CreateDirectory(directory);
            remoteTargetDirectory = $"{remoteWorkingFolder}{CopyIgnoreFTPConstants.unixPathSeparator}{directory}";
        }
        ftpClient.SetWorkingDirectory(remoteTargetDirectory);
        ftpClient.UploadDirectory(localFolder, ftpClient.GetWorkingDirectory(), existsMode: FtpRemoteExists.Overwrite, progress: callback);
        ftpClient.SetWorkingDirectory(remoteWorkingFolder);
    }

    public void UploadFile(string filePath, Action<FtpProgress> callback)
    {
        ArgumentNullException.ThrowIfNull(Info);
        Connect(Info);
        ftpClient.UploadFile(filePath, ftpClient.GetWorkingDirectory(), progress: callback);
    }

    public FtpListItem[] GetListing(string directory)
    {
        ArgumentNullException.ThrowIfNull(Info);
        Connect(Info);
        ftpClient.SetWorkingDirectory(directory);
        return ftpClient.GetListing();
    }

    public string GetWorkingDirectory()
    {
        if (!ftpClient.IsConnected)
        {
            ArgumentNullException.ThrowIfNull(Info);
            Connect(Info);
        }
        return ftpClient.GetWorkingDirectory();
    }

    public void SetWorkingDirectory(string directory)
    {
        ftpClient.SetWorkingDirectory(directory);
    }

    public void Dispose()
    {
        ftpClient.Dispose();
        GC.SuppressFinalize(this);
    }
}
