﻿namespace CopyIgnore.FTP;

public class CopyIgnoreFTPInfo
{
    public int Port { get; set; } = 21;
    public string Host { get; set; } = "127.0.0.1";
    public string Password { get; set; } = string.Empty;
    public string Username { get; set; } = string.Empty;
}
