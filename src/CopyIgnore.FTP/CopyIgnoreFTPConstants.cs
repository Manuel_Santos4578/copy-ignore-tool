﻿namespace CopyIgnore.FTP;

public class CopyIgnoreFTPConstants
{
    public const char unixPathSeparator = '/';
    public const char windowsPathSeparator = '\\';
    public const string navigateUp = "..";
}
