﻿namespace cpi;
internal static class Helpers
{
    internal static class CommandLine
    {
        internal static Dictionary<string, string> ParseArguments(string[] args)
        {
            Dictionary<string, string> arguments = [];

            for (int i = 0; i < args.Length; i++)
            {
                string arg = args[i];

                // Check if the argument has the format "-key=value"
                if (arg.StartsWith('-') && arg.Contains('='))
                {
                    // Split the argument into key and value
                    string[] parts = arg.Split('=');
                    string key = parts[0][1..]; // Remove the leading "-"
                    string value = parts[1];

                    // Store the key-value pair in the dictionary
                    arguments[key] = value;
                }
            }
            return arguments;
        }
    }
}
