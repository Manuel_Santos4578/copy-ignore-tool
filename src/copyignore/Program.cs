﻿using Copyignore.Shared;
using Copyignore.Shared.FileSystemHelpers;
using cpi;
using System.Diagnostics;

Console.WriteLine("Starting Execution...");

Dictionary<string, string> arguments = Helpers.CommandLine.ParseArguments(args);
string workDir = Environment.CurrentDirectory;
bool containsTarget = arguments.ContainsKey(AppConstants.Args.TargetDir);

if (!containsTarget)
{
    Console.WriteLine("missing args -target");
    return;
}
bool targetExists = Directory.Exists(arguments[AppConstants.Args.TargetDir]);

if (!targetExists)
{
    Console.WriteLine("target dir does not exist");
    return;
}

string target = arguments[AppConstants.Args.TargetDir];
string ignoreFileName = ".copyignore";
string ignoreFilePath = Path.GetFullPath($"{workDir}{Path.DirectorySeparatorChar}{Path.DirectorySeparatorChar}{ignoreFileName}");
bool ignoreFileExists = File.Exists(ignoreFilePath);
if (!ignoreFileExists)
{
    Console.WriteLine("ignore file does not exist");
    return;
}

if (arguments.TryGetValue(AppConstants.Args.IgnoreFile, out string? value))
{
    ignoreFilePath = Path.GetFullPath($"{workDir}{Path.DirectorySeparatorChar}{Path.DirectorySeparatorChar}{value}");
    Console.WriteLine($"Ignore file {workDir}{Path.DirectorySeparatorChar}{ignoreFilePath}");
}

Console.WriteLine("Reading Target Folder...");
IEnumerable<string> entries = Directory.EnumerateFileSystemEntries(target);
if (entries.Any())
{
    Console.Write("Delete target folder contents? [Y/n]: ");
    string? reply = Console.ReadLine();
    if (string.Compare(reply, "y", StringComparison.OrdinalIgnoreCase) == 0)
    {
        DirectoryInfo dir = new(target);
        foreach (FileInfo file in dir.GetFiles())
        {
            file.Delete();
        }
        foreach (DirectoryInfo subDir in dir.GetDirectories())
        {
            Directory.Delete(subDir.FullName, true);
        }
    }
}

var st = Stopwatch.StartNew();

Console.WriteLine("Reading Ignore File...");
FileSystemHelper.IgnoreFile.BuildRegexCacheFromIgnoreFile(ignoreFilePath);

Console.WriteLine("Reading Directories...");
List<string> sourceDirs = FileSystemHelper.Directory.GetDirectoryPaths($"{workDir}{Path.DirectorySeparatorChar}");
Console.WriteLine(st.Elapsed);

Console.WriteLine("Copying directories...");
await FolderProcessor.BackupFilesParallel(workDir, target, sourceDirs);
Console.WriteLine("Directory copied sucessfully");

st.Stop();
Console.WriteLine(st.Elapsed);