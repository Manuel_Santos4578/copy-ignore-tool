﻿namespace cpi;

internal class AppConstants
{
    internal class Args
    {
        internal const string SourceDir = "source";
        internal const string TargetDir = "target";
        internal const string IgnoreFile = "ignoreFile";
    }
}
