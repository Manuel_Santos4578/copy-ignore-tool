using FTPClientWithIgnore.Configuration;
using FTPClientWithIgnore.State;
using System.Text.Json;

namespace FTPClientWithIgnore;

internal static class Program
{
    /// <summary>
    ///  The main entry point for the application.
    /// </summary>
    [STAThread]
    static void Main()
    {
        string workingDirFile = Path.Combine(AppContext.BaseDirectory, "lastworkingdir.json");
        AppConfiguration configuration;
        if (!File.Exists(workingDirFile))
        {
            using FileStream file = File.Create(workingDirFile);
            file.Close();
            configuration = new()
            {
                WorkingDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),

            };
            File.WriteAllText(workingDirFile, JsonSerializer.Serialize(configuration));
        }
        else
        {
            string fileText = File.ReadAllText(workingDirFile);
            configuration = JsonSerializer.Deserialize<AppConfiguration>(fileText)!;
        }
        FTPConnectionsState.CurrentWorkDirectory = configuration.WorkingDirectory!;
        // To customize application configuration such as set high DPI settings or default font,
        // see https://aka.ms/applicationconfiguration.
        ApplicationConfiguration.Initialize();
        Application.ApplicationExit += (sender, eventArgs) =>
        {
            File.WriteAllText(workingDirFile, JsonSerializer.Serialize(configuration));
        };
        Application.Run(new MainForm());
    }
}