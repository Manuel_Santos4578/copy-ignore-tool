using FTPClientWithIgnore.Screens;

namespace FTPClientWithIgnore;

internal partial class MainForm : Form
{
    internal MainForm()
    {
        InitializeComponent();
        Text = "Ftp Client with ignore";
    }

    private void MainForm_Load(object sender, EventArgs e)
    {
        new UploadForm
        {
            MdiParent = this
        }.Show();
    }
}
