﻿namespace FTPClientWithIgnore.State;

internal static class FTPConnectionsState
{
    private static string? _currentWorkDirectory;
    private static readonly object _lockObject = new();
    private static readonly object _remoteLockObject = new();

    public static string CurrentWorkDirectory
    {
        get
        {
            lock (_lockObject)
            {
                return _currentWorkDirectory ?? string.Empty;
            }
        }
        set
        {
            lock (_lockObject)
            {
                _currentWorkDirectory = value;
            }
        }
    }

    private static string? _remoteWorkDirectory;

    public static string? RemoteWorkDirectory
    {
        get
        {
            lock (_remoteLockObject)
            {
                return _remoteWorkDirectory ?? string.Empty;
            }
        }
        set
        {
            lock (_remoteLockObject)
            {
                _remoteWorkDirectory = value;
            }
        }
    }
}
