﻿namespace FTPClientWithIgnore.Screens
{
    partial class UploadForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UploadForm));
            panel_connection = new Panel();
            button_connect = new Button();
            textBox_port = new TextBox();
            textBox_password = new TextBox();
            textBox_username = new TextBox();
            label_host = new Label();
            label_port = new Label();
            label_username = new Label();
            textBox_host = new TextBox();
            label_password = new Label();
            panel_feedback = new Panel();
            listBox_processedFiles = new ListBox();
            textBox_local_workingdirectory = new TextBox();
            panel1 = new Panel();
            panel3 = new Panel();
            panel_local = new Panel();
            splitContainer1 = new SplitContainer();
            listBox_local_workingdirectory = new ListBox();
            textBox_remote_directory = new TextBox();
            listBox_remote_workingdirectory = new ListBox();
            imageList = new ImageList(components);
            panel_connection.SuspendLayout();
            panel_feedback.SuspendLayout();
            panel_local.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splitContainer1).BeginInit();
            splitContainer1.Panel1.SuspendLayout();
            splitContainer1.Panel2.SuspendLayout();
            splitContainer1.SuspendLayout();
            SuspendLayout();
            // 
            // panel_connection
            // 
            panel_connection.Controls.Add(button_connect);
            panel_connection.Controls.Add(textBox_port);
            panel_connection.Controls.Add(textBox_password);
            panel_connection.Controls.Add(textBox_username);
            panel_connection.Controls.Add(label_host);
            panel_connection.Controls.Add(label_port);
            panel_connection.Controls.Add(label_username);
            panel_connection.Controls.Add(textBox_host);
            panel_connection.Controls.Add(label_password);
            panel_connection.Dock = DockStyle.Top;
            panel_connection.Location = new Point(0, 0);
            panel_connection.Margin = new Padding(0);
            panel_connection.MaximumSize = new Size(0, 40);
            panel_connection.MinimumSize = new Size(0, 30);
            panel_connection.Name = "panel_connection";
            panel_connection.Size = new Size(1218, 40);
            panel_connection.TabIndex = 0;
            // 
            // button_connect
            // 
            button_connect.BackColor = Color.Transparent;
            button_connect.FlatStyle = FlatStyle.Flat;
            button_connect.Location = new Point(708, 9);
            button_connect.Name = "button_connect";
            button_connect.Size = new Size(123, 24);
            button_connect.TabIndex = 5;
            button_connect.Text = "Quickconnect";
            button_connect.UseVisualStyleBackColor = false;
            button_connect.Click += Button_connect_Click;
            // 
            // textBox_port
            // 
            textBox_port.Location = new Point(626, 9);
            textBox_port.Name = "textBox_port";
            textBox_port.Size = new Size(57, 23);
            textBox_port.TabIndex = 4;
            textBox_port.KeyUp += TextBox_KeyUp;
            // 
            // textBox_password
            // 
            textBox_password.Location = new Point(455, 9);
            textBox_password.Name = "textBox_password";
            textBox_password.PasswordChar = '*';
            textBox_password.Size = new Size(127, 23);
            textBox_password.TabIndex = 3;
            textBox_password.KeyUp += TextBox_KeyUp;
            // 
            // textBox_username
            // 
            textBox_username.Location = new Point(258, 9);
            textBox_username.Name = "textBox_username";
            textBox_username.Size = new Size(127, 23);
            textBox_username.TabIndex = 2;
            textBox_username.KeyUp += TextBox_KeyUp;
            // 
            // label_host
            // 
            label_host.AutoSize = true;
            label_host.Location = new Point(12, 12);
            label_host.Name = "label_host";
            label_host.Size = new Size(35, 15);
            label_host.TabIndex = 2;
            label_host.Text = "Host:";
            // 
            // label_port
            // 
            label_port.AutoSize = true;
            label_port.Location = new Point(588, 12);
            label_port.Name = "label_port";
            label_port.Size = new Size(32, 15);
            label_port.TabIndex = 5;
            label_port.Text = "Port:";
            // 
            // label_username
            // 
            label_username.AutoSize = true;
            label_username.Location = new Point(189, 12);
            label_username.Name = "label_username";
            label_username.Size = new Size(63, 15);
            label_username.TabIndex = 3;
            label_username.Text = "Username:";
            // 
            // textBox_host
            // 
            textBox_host.Location = new Point(56, 9);
            textBox_host.Name = "textBox_host";
            textBox_host.Size = new Size(127, 23);
            textBox_host.TabIndex = 1;
            textBox_host.KeyUp += TextBox_KeyUp;
            // 
            // label_password
            // 
            label_password.AutoSize = true;
            label_password.Location = new Point(389, 12);
            label_password.Name = "label_password";
            label_password.Size = new Size(60, 15);
            label_password.TabIndex = 4;
            label_password.Text = "Password:";
            // 
            // panel_feedback
            // 
            panel_feedback.Controls.Add(listBox_processedFiles);
            panel_feedback.Dock = DockStyle.Top;
            panel_feedback.Location = new Point(0, 40);
            panel_feedback.Margin = new Padding(0);
            panel_feedback.Name = "panel_feedback";
            panel_feedback.Padding = new Padding(5, 5, 5, 2);
            panel_feedback.Size = new Size(1218, 100);
            panel_feedback.TabIndex = 1;
            // 
            // listBox_processedFiles
            // 
            listBox_processedFiles.Dock = DockStyle.Fill;
            listBox_processedFiles.FormattingEnabled = true;
            listBox_processedFiles.ItemHeight = 15;
            listBox_processedFiles.Location = new Point(5, 5);
            listBox_processedFiles.Name = "listBox_processedFiles";
            listBox_processedFiles.Size = new Size(1208, 93);
            listBox_processedFiles.TabIndex = 0;
            // 
            // textBox_local_workingdirectory
            // 
            textBox_local_workingdirectory.Dock = DockStyle.Top;
            textBox_local_workingdirectory.Location = new Point(3, 396);
            textBox_local_workingdirectory.Name = "textBox_local_workingdirectory";
            textBox_local_workingdirectory.Size = new Size(606, 23);
            textBox_local_workingdirectory.TabIndex = 0;
            // 
            // panel1
            // 
            panel1.Dock = DockStyle.Bottom;
            panel1.Location = new Point(0, 634);
            panel1.Margin = new Padding(3, 2, 3, 2);
            panel1.Name = "panel1";
            panel1.Size = new Size(1218, 53);
            panel1.TabIndex = 2;
            // 
            // panel3
            // 
            panel3.Dock = DockStyle.Bottom;
            panel3.Location = new Point(0, 575);
            panel3.Margin = new Padding(3, 2, 3, 2);
            panel3.Name = "panel3";
            panel3.Size = new Size(1218, 59);
            panel3.TabIndex = 3;
            // 
            // panel_local
            // 
            panel_local.Controls.Add(splitContainer1);
            panel_local.Dock = DockStyle.Fill;
            panel_local.Location = new Point(0, 140);
            panel_local.Margin = new Padding(3, 2, 3, 2);
            panel_local.Name = "panel_local";
            panel_local.Padding = new Padding(4);
            panel_local.Size = new Size(1218, 435);
            panel_local.TabIndex = 4;
            // 
            // splitContainer1
            // 
            splitContainer1.Dock = DockStyle.Fill;
            splitContainer1.Location = new Point(4, 4);
            splitContainer1.Margin = new Padding(3, 2, 3, 2);
            splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            splitContainer1.Panel1.Controls.Add(textBox_local_workingdirectory);
            splitContainer1.Panel1.Controls.Add(listBox_local_workingdirectory);
            splitContainer1.Panel1.Padding = new Padding(3, 2, 3, 2);
            // 
            // splitContainer1.Panel2
            // 
            splitContainer1.Panel2.Controls.Add(textBox_remote_directory);
            splitContainer1.Panel2.Controls.Add(listBox_remote_workingdirectory);
            splitContainer1.Panel2.Padding = new Padding(3, 2, 3, 2);
            splitContainer1.Size = new Size(1210, 427);
            splitContainer1.SplitterDistance = 612;
            splitContainer1.TabIndex = 0;
            // 
            // listBox_local_workingdirectory
            // 
            listBox_local_workingdirectory.BackColor = SystemColors.Window;
            listBox_local_workingdirectory.Dock = DockStyle.Top;
            listBox_local_workingdirectory.DrawMode = DrawMode.OwnerDrawFixed;
            listBox_local_workingdirectory.FormattingEnabled = true;
            listBox_local_workingdirectory.ItemHeight = 15;
            listBox_local_workingdirectory.Location = new Point(3, 2);
            listBox_local_workingdirectory.Margin = new Padding(3, 2, 3, 2);
            listBox_local_workingdirectory.Name = "listBox_local_workingdirectory";
            listBox_local_workingdirectory.Size = new Size(606, 394);
            listBox_local_workingdirectory.TabIndex = 0;
            listBox_local_workingdirectory.DrawItem += ListBox_local_working_directory_DrawItem;
            listBox_local_workingdirectory.KeyUp += ListBox_local_workingdirectory_KeyUp;
            listBox_local_workingdirectory.MouseDoubleClick += ListBox_local_workingdirectory_MouseDoubleClick;
            // 
            // textBox_remote_directory
            // 
            textBox_remote_directory.Dock = DockStyle.Top;
            textBox_remote_directory.Location = new Point(3, 396);
            textBox_remote_directory.Name = "textBox_remote_directory";
            textBox_remote_directory.Size = new Size(588, 23);
            textBox_remote_directory.TabIndex = 1;
            // 
            // listBox_remote_workingdirectory
            // 
            listBox_remote_workingdirectory.Dock = DockStyle.Top;
            listBox_remote_workingdirectory.DrawMode = DrawMode.OwnerDrawFixed;
            listBox_remote_workingdirectory.FormattingEnabled = true;
            listBox_remote_workingdirectory.ItemHeight = 15;
            listBox_remote_workingdirectory.Location = new Point(3, 2);
            listBox_remote_workingdirectory.Margin = new Padding(3, 2, 3, 2);
            listBox_remote_workingdirectory.Name = "listBox_remote_workingdirectory";
            listBox_remote_workingdirectory.Size = new Size(588, 394);
            listBox_remote_workingdirectory.TabIndex = 0;
            listBox_remote_workingdirectory.DrawItem += ListBox_remote_workingdirectory_DrawItem;
            listBox_remote_workingdirectory.MouseDoubleClick += ListBox_remote_workingdirectory_MouseDoubleClick;
            // 
            // imageList
            // 
            imageList.ColorDepth = ColorDepth.Depth32Bit;
            imageList.ImageStream = (ImageListStreamer)resources.GetObject("imageList.ImageStream");
            imageList.TransparentColor = Color.Transparent;
            imageList.Images.SetKeyName(0, "cake.png");
            imageList.Images.SetKeyName(1, "folder.png");
            imageList.Images.SetKeyName(2, "file.png");
            // 
            // UploadForm
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(1218, 687);
            ControlBox = false;
            Controls.Add(panel_local);
            Controls.Add(panel3);
            Controls.Add(panel1);
            Controls.Add(panel_feedback);
            Controls.Add(panel_connection);
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "UploadForm";
            Text = "Upload form";
            WindowState = FormWindowState.Maximized;
            FormClosing += UploadForm_FormClosing;
            panel_connection.ResumeLayout(false);
            panel_connection.PerformLayout();
            panel_feedback.ResumeLayout(false);
            panel_local.ResumeLayout(false);
            splitContainer1.Panel1.ResumeLayout(false);
            splitContainer1.Panel1.PerformLayout();
            splitContainer1.Panel2.ResumeLayout(false);
            splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)splitContainer1).EndInit();
            splitContainer1.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private Panel panel_connection;
        private Panel panel_feedback;
        private Label label_host;
        private Label label_port;
        private Label label_username;
        private TextBox textBox_host;
        private Label label_password;
        private TextBox textBox_port;
        private TextBox textBox_password;
        private TextBox textBox_username;
        private Button button_connect;
        private Panel panel1;
        private Panel panel3;
        private Panel panel_local;
        private SplitContainer splitContainer1;
        private ListBox listBox_local_workingdirectory;
        private ListBox listBox_remote_workingdirectory;
        private TextBox textBox_local_workingdirectory;
        private TextBox textBox_remote_directory;
        private ImageList imageList;
        private ListBox listBox_processedFiles;
    }
}