﻿using Copyignore.Shared;
using Copyignore.Shared.FileSystemHelpers;
using CopyIgnore.FTP;
using FluentFTP;
using FTPClientWithIgnore.Configuration;
using FTPClientWithIgnore.State;

namespace FTPClientWithIgnore.Screens;

internal partial class UploadForm : Form
{
    private readonly CopyIgnoreFTPClient ftpClient;
    private const string _navigateUp = "..";
    //private const int folderIconIndex = 0;
    private readonly Lock _ftpLock = new();

    CopyIgnoreFTPInfo _info;
    internal UploadForm()
    {
        InitializeComponent();
        textBox_local_workingdirectory.Text = FTPConnectionsState.CurrentWorkDirectory;
        _info = new CopyIgnoreFTPInfo
        {
            Host = textBox_host.Text,
            Port = int.TryParse(textBox_port.Text, out int port) ? port : 21,
            Username = textBox_username.Text,
            Password = textBox_password.Text,
        };
        ftpClient = new CopyIgnoreFTPClient(_info);
        ListLocalDirectory();
    }

    #region methods
    private void Connect()
    {
        _info = new CopyIgnoreFTPInfo
        {
            Host = textBox_host.Text,
            Port = int.TryParse(textBox_port.Text, out int port) ? port : 21,
            Username = textBox_username.Text,
            Password = textBox_password.Text,
        };
        ftpClient.UpdateInfo(_info);
    }

    private void Disconnect()
    {
        ftpClient.Disconnect();
    }

    private void ListLocalDirectory()
    {
        textBox_local_workingdirectory.Text = FTPConnectionsState.CurrentWorkDirectory;
        string[] content = [.. Directory.EnumerateFileSystemEntries(FTPConnectionsState.CurrentWorkDirectory)
            .OrderBy(e => !Directory.Exists(Path.Combine(FTPConnectionsState.CurrentWorkDirectory, Path.GetFileName(e))))
            .Select(e => Path.GetFileName(e))];
        listBox_local_workingdirectory.Items.Clear();
        bool isRoot = new DirectoryInfo(FTPConnectionsState.CurrentWorkDirectory).Parent is null;
        if (!isRoot)
        {
            listBox_local_workingdirectory.Items.Add(_navigateUp);
        }
        listBox_local_workingdirectory.Items.AddRange(content);
        listBox_local_workingdirectory.Update();
    }

    private void ListDirectories(string directory)
    {
        FtpListItem[] items = [.. ftpClient.GetListing(directory).OrderBy(e => e.Type != FtpObjectType.Directory)];

        listBox_remote_workingdirectory.Items.Clear();
        listBox_remote_workingdirectory.ValueMember = nameof(FtpListItem.Name);
        listBox_remote_workingdirectory.DisplayMember = nameof(FtpListItem.Name);
        listBox_remote_workingdirectory.Items.Add(new FtpListItem { Name = _navigateUp });
        listBox_remote_workingdirectory.Items.AddRange(items);
        listBox_remote_workingdirectory.Update();

        textBox_remote_directory.Text = FTPConnectionsState.RemoteWorkDirectory = ftpClient.GetWorkingDirectory();
    }

    private void FtpUpload(string directoryName, string localFolder)
    {
        listBox_processedFiles.Items.Clear();
        try
        {
            if (string.IsNullOrWhiteSpace(FTPConnectionsState.RemoteWorkDirectory))
            {
                listBox_processedFiles.Items.Add("Remote Path missing");
                listBox_processedFiles.Update();
                return;
            }
            string newDirectory = Path.Combine(AppContext.BaseDirectory, directoryName);
            Directory.Move(localFolder, newDirectory);
            ftpClient.UploadFolder(newDirectory, remoteWorkingFolder: FTPConnectionsState.RemoteWorkDirectory, progress =>
            {
                listBox_processedFiles.Items.Add($"Uploaded {progress.LocalPath[localFolder.Length..]}");
                listBox_processedFiles.Update();
            });
            listBox_processedFiles.Items.Add("Folder processed");
            listBox_processedFiles.Update();
            Directory.Delete(newDirectory, true);
        }
        catch (Exception ex)
        {
            listBox_processedFiles.Items.Add($"An error occurred, {ex.Message}");
            listBox_processedFiles.Update();
        }
    }

    private void FtpConnect()
    {
        if (string.IsNullOrWhiteSpace(textBox_host.Text)
            || string.IsNullOrWhiteSpace(textBox_username.Text)
            || string.IsNullOrWhiteSpace(textBox_password.Text))
        {
            listBox_processedFiles.Items.Add($"Missing data for ftp connection.");
            return;
        }
        try
        {
            listBox_processedFiles.Items.Add($"Attempting connection to {textBox_host.Text}:{textBox_port.Text}.");
            _info = new CopyIgnoreFTPInfo
            {
                Host = textBox_host.Text,
                Username = textBox_username.Text,
                Password = textBox_password.Text,
                Port = int.TryParse(textBox_port.Text, out int port) ? port : 21,
            };
            ftpClient.UpdateInfo(_info);
            Connect();
        }
        catch (Exception)
        {
            listBox_processedFiles.Items.Add($"Could not connect to {textBox_host.Text}:{textBox_port.Text}");
            return;
        }

        try
        {
            ListDirectories(ftpClient.GetWorkingDirectory());
        }
        catch (Exception)
        {
            listBox_processedFiles.Items.Add($"Could not connect to {textBox_host.Text}:{textBox_port.Text}");
        }
    }

    #endregion

    #region events
    private void Button_connect_Click(object sender, EventArgs e)
    {
        FtpConnect();
    }

    private void UploadForm_FormClosing(object sender, FormClosingEventArgs e)
    {
        Disconnect();
        ftpClient.Dispose();
    }

    private void ListBox_local_workingdirectory_MouseDoubleClick(object sender, MouseEventArgs e)
    {
        string workDir = FTPConnectionsState.CurrentWorkDirectory;
        if (listBox_local_workingdirectory.SelectedIndex == -1)
        {
            return;
        }
        FTPConnectionsState.CurrentWorkDirectory = Path.GetFullPath(Path.Combine(textBox_local_workingdirectory.Text, listBox_local_workingdirectory.SelectedItem!.ToString()!));
        try
        {
            ListLocalDirectory();
        }
        catch (Exception)
        {
            textBox_local_workingdirectory.Text = FTPConnectionsState.CurrentWorkDirectory = workDir;
        }
    }

    private void ListBox_local_working_directory_DrawItem(object sender, DrawItemEventArgs e)
    {
        e.DrawBackground();
        if (e.Index >= 0)
        {
            string itemText = listBox_local_workingdirectory.Items[e.Index].ToString()!;

            Brush brush = e.State.HasFlag(DrawItemState.Focus) ? Brushes.White : Brushes.Black;
            if (Directory.Exists(Path.Combine(FTPConnectionsState.CurrentWorkDirectory, itemText)))
            {
                e.Graphics.DrawImage(imageList.Images["folder.png"]!, e.Bounds.Left, e.Bounds.Top);
                e.Graphics.DrawString(itemText, e.Font!, brush, e.Bounds.Left + imageList.ImageSize.Width, e.Bounds.Top);
            }
            else
            {
                e.Graphics.DrawImage(imageList.Images["file.png"]!, e.Bounds.Left, e.Bounds.Top);
                e.Graphics.DrawString(itemText, e.Font!, brush, e.Bounds.Left + imageList.ImageSize.Width, e.Bounds.Top);
            }
        }

        e.DrawFocusRectangle();
    }

    private void ListBox_remote_workingdirectory_MouseDoubleClick(object sender, MouseEventArgs e)
    {
        ListBox box = (ListBox)sender;
        string navigateTo = ((FtpListItem)box.SelectedItem!).Name;
        string workingDirectory = ftpClient.GetWorkingDirectory();
        string remoteP;

        if (navigateTo == _navigateUp)
        {
            char sep = ftpClient.ServerOS == FtpOperatingSystem.Unix ? CopyIgnoreFTPConstants.unixPathSeparator : CopyIgnoreFTPConstants.windowsPathSeparator;
            string[] paths = workingDirectory.Split(sep);
            if (paths.Length > 1)
            {
                string[] newPaths = paths[..(paths.Length - 1)];
                remoteP = string.Join(sep, newPaths);
            }
            else
            {
                remoteP = string.Join(sep, paths);
            }
        }
        else
        {
            remoteP = Path.Combine(workingDirectory, navigateTo);
        }
        textBox_remote_directory.Text = FTPConnectionsState.RemoteWorkDirectory = remoteP;
        ListDirectories(FTPConnectionsState.RemoteWorkDirectory);
    }

    private void ListBox_remote_workingdirectory_DrawItem(object sender, DrawItemEventArgs e)
    {
        e.DrawBackground();
        if (e.Index >= 0)
        {
            FtpListItem item = (FtpListItem)listBox_remote_workingdirectory.Items[e.Index];
            string itemText = item.Name.ToString();
            Brush brush = e.State.HasFlag(DrawItemState.Focus) ? Brushes.White : Brushes.Black;
            if (item.Type == FtpObjectType.Directory || item.Type == FtpObjectType.Link)
            {
                e.Graphics.DrawImage(imageList.Images["folder.png"]!, e.Bounds.Left, e.Bounds.Top);
                e.Graphics.DrawString(itemText, e.Font!, brush, e.Bounds.Left + imageList.ImageSize.Width, e.Bounds.Top);
            }
            else
            {
                e.Graphics.DrawImage(imageList.Images["file.png"]!, e.Bounds.Left, e.Bounds.Top);
                e.Graphics.DrawString(itemText, e.Font!, brush, e.Bounds.Left + imageList.ImageSize.Width, e.Bounds.Top);
            }
        }

        e.DrawFocusRectangle();
    }
    #endregion

    private async void ListBox_local_workingdirectory_KeyUp(object sender, KeyEventArgs e)
    {
        if (e.KeyCode == Keys.Right && e.Modifiers == Keys.Alt)
        {
            lock (_ftpLock)
            {
                if (listBox_local_workingdirectory.SelectedIndex == -1)
                {
                    listBox_processedFiles.Items.Add($"Must select a local directory to upload");
                    return;
                }
                string entry = Path.GetFullPath(Path.Combine(textBox_local_workingdirectory.Text, listBox_local_workingdirectory.SelectedItem!.ToString()!));
                FileAttributes att = File.GetAttributes(entry);
                bool isDir = att.HasFlag(FileAttributes.Directory);
                if (isDir)
                {
                    string ignoreFilePath = Path.Combine(entry, AppConfiguration.DEFAULT_IGNORE_FILE);
                    if (Directory.Exists(AppConfiguration.IntermediateDir))
                    {
                        Directory.Delete(AppConfiguration.IntermediateDir, true);
                    }
                    _ = Directory.CreateDirectory(AppConfiguration.IntermediateDir);
                    FileSystemHelper.IgnoreFile.BuildRegexCacheFromIgnoreFile(ignoreFilePath);
                    List<string> directories = FileSystemHelper.Directory.GetDirectoryPaths(entry);

                    FolderProcessor.BackupFilesParallelSync(entry, AppConfiguration.IntermediateDir, directories);
                    FtpUpload(Path.GetFileName(entry), AppConfiguration.IntermediateDir);
                }
                else
                {
                    //TODO: fix this
                    //ftpClient.UploadFile(entry, progress =>
                    //{
                    //    listBox_processedFiles.Items.Add($"Uploaded {entry}");
                    //    listBox_processedFiles.Update();
                    //});
                }
                listBox_processedFiles.Items.Add("Done");
                ListDirectories(ftpClient.GetWorkingDirectory());
            }
        }
        else if (e.KeyCode == Keys.Return)
        {
            string workDir = FTPConnectionsState.CurrentWorkDirectory;
            if (listBox_local_workingdirectory.SelectedIndex == -1)
            {
                return;
            }
            FTPConnectionsState.CurrentWorkDirectory = Path.GetFullPath(Path.Combine(textBox_local_workingdirectory.Text, listBox_local_workingdirectory.SelectedItem!.ToString()!));
            try
            {
                ListLocalDirectory();
            }
            catch (Exception)
            {
                textBox_local_workingdirectory.Text = FTPConnectionsState.CurrentWorkDirectory = workDir;
            }
        }
        await Task.CompletedTask;
    }

    private void TextBox_KeyUp(object sender, KeyEventArgs e)
    {
        if (e.KeyCode == Keys.Enter)
        {
            FtpConnect();
        }
    }
}
