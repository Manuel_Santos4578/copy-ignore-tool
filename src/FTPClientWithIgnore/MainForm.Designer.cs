﻿namespace FTPClientWithIgnore
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            menuStrip = new MenuStrip();
            fileToolStripMenuItem = new ToolStripMenuItem();
            siteManagerToolStripMenuItem = new ToolStripMenuItem();
            copyCurrentConnectionToSiteManagerToolStripMenuItem = new ToolStripMenuItem();
            menuStrip.SuspendLayout();
            SuspendLayout();
            // 
            // menuStrip
            // 
            menuStrip.Items.AddRange(new ToolStripItem[] { fileToolStripMenuItem });
            menuStrip.Location = new Point(0, 0);
            menuStrip.Name = "menuStrip";
            menuStrip.Size = new Size(800, 24);
            menuStrip.TabIndex = 1;
            menuStrip.Text = "menuStrip";
            // 
            // fileToolStripMenuItem
            // 
            fileToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] { siteManagerToolStripMenuItem, copyCurrentConnectionToSiteManagerToolStripMenuItem });
            fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            fileToolStripMenuItem.Size = new Size(37, 20);
            fileToolStripMenuItem.Text = "File";
            // 
            // siteManagerToolStripMenuItem
            // 
            siteManagerToolStripMenuItem.Name = "siteManagerToolStripMenuItem";
            siteManagerToolStripMenuItem.Size = new Size(292, 22);
            siteManagerToolStripMenuItem.Text = "Site Manager";
            // 
            // copyCurrentConnectionToSiteManagerToolStripMenuItem
            // 
            copyCurrentConnectionToSiteManagerToolStripMenuItem.Name = "copyCurrentConnectionToSiteManagerToolStripMenuItem";
            copyCurrentConnectionToSiteManagerToolStripMenuItem.Size = new Size(292, 22);
            copyCurrentConnectionToSiteManagerToolStripMenuItem.Text = "Copy current connection to Site Manager";
            // 
            // MainForm
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Controls.Add(menuStrip);
            Icon = (Icon)resources.GetObject("$this.Icon");
            IsMdiContainer = true;
            MainMenuStrip = menuStrip;
            Name = "MainForm";
            Text = "Ftp Client with ignore";
            WindowState = FormWindowState.Maximized;
            Load += MainForm_Load;
            menuStrip.ResumeLayout(false);
            menuStrip.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private MenuStrip menuStrip;
        private ToolStripMenuItem fileToolStripMenuItem;
        private ToolStripMenuItem siteManagerToolStripMenuItem;
        private ToolStripMenuItem copyCurrentConnectionToSiteManagerToolStripMenuItem;
    }
}
