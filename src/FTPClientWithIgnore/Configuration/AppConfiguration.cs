﻿namespace FTPClientWithIgnore.Configuration;

internal class AppConfiguration
{
    public const string TEMP_WORKDIR = "Temp";
    public const string DEFAULT_IGNORE_FILE = ".copyignore";

    public string? WorkingDirectory { get; set; }
    public static string IntermediateDir => Path.Combine(AppContext.BaseDirectory, TEMP_WORKDIR);
}
