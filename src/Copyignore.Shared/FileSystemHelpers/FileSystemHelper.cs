﻿using System.Runtime.InteropServices;
using System.Text.RegularExpressions;

namespace Copyignore.Shared.FileSystemHelpers;

public class FileSystemHelper
{
    public class Directory
    {

        public static List<string> GetDirectoryPaths(string path)
        {
            List<string> directoryPaths = [];

            Stack<string> stack = new();
            stack.Push(path);

            while (stack.Count > 0)
            {
                string currentPath = stack.Pop();

                try
                {
                    bool shouldSkip = Regex.RegexCache.Any(r => r.IsMatch(currentPath));
                    if (shouldSkip)
                    {
                        continue;
                    }

                    directoryPaths.Add(currentPath);

                    string[] directories = System.IO.Directory.GetDirectories(currentPath);
                    foreach (string directory in directories)
                    {
                        stack.Push(directory);
                    }
                }
                catch (UnauthorizedAccessException ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            return directoryPaths;
        }
    }

    public class IgnoreFile
    {

        public static void BuildRegexCacheFromIgnoreFile(string ignoreFilePath)
        {

            List<string> regexExps = [];
            bool ignoreFileExists = File.Exists(ignoreFilePath);
            if (ignoreFileExists)
            {
                string content = File.ReadAllText(ignoreFilePath);
                IEnumerable<string> lines = content.Split("\r\n", StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries).Where(l => !l.StartsWith('#'));
                regexExps.AddRange(lines);
            }
            Regex.RegexCache = new(regexExps.Count);
            foreach (string line in regexExps)
            {
                Regex.SetRegex(Regex.WildcardToRegex(line), RegexOptions.IgnoreCase | RegexOptions.Compiled);
            }
        }
    }

    public class Regex
    {
        public static List<System.Text.RegularExpressions.Regex> RegexCache = [];

        public static void SetRegex(string pattern, RegexOptions options)
        {
            RegexCache.Add(new System.Text.RegularExpressions.Regex(pattern, options));
        }

        public static string WildcardToRegex(string wildcardPattern)
        {
            string pattern = System.Text.RegularExpressions.Regex.Escape(wildcardPattern)
                .Replace("\\*", ".*")
                .Replace("/*", ".*")
                .Replace("\\?", ".")
                .Replace("/?", ".");
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                pattern = pattern.Replace("/", "\\\\");
            }
            return pattern;
        }
    }
}
