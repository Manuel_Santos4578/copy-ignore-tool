﻿using Copyignore.Shared.FileSystemHelpers;

namespace Copyignore.Shared;

public static class FolderProcessor
{
    public static async Task BackupFilesParallel(string workDir, string targetDir, IEnumerable<string> toCopy)
    {
        await Parallel.ForEachAsync(toCopy, async (dir, cancelToken) =>
        {
            string partialDir = dir[workDir.Length..];

            if (partialDir.StartsWith($"{Path.DirectorySeparatorChar}.{Path.DirectorySeparatorChar}"))
            {
                partialDir = partialDir[3..];
            }
            string directoryToCreate = $"{targetDir}{partialDir}";

            if (!Directory.Exists(directoryToCreate))
            {
                //Console.WriteLine($"Creating Dir {directoryToCreate}");
                Directory.CreateDirectory(directoryToCreate);
            }
            IEnumerable<string> files = Directory.EnumerateFiles(dir);

            await Parallel.ForEachAsync(files, async (file, token) =>
            {
                string partialSourceFilePath = file[workDir.Length..];
                if (partialSourceFilePath.StartsWith($"{Path.DirectorySeparatorChar}.{Path.DirectorySeparatorChar}"))
                {
                    partialSourceFilePath = partialSourceFilePath[3..];
                }
                string targetFilePath = $"{targetDir}{partialSourceFilePath}";

                bool found = FileSystemHelper.Regex.RegexCache.Any(r => r.IsMatch(partialSourceFilePath));
                if (!found)
                {
                    File.Copy(
                        Path.GetFullPath($"{workDir}{Path.DirectorySeparatorChar}{partialSourceFilePath}"),
                        $"{targetFilePath}", true);
                }
                await ValueTask.CompletedTask;
            });
        });
    }

    public static void BackupFilesParallelSync(string workDir, string targetDir, IEnumerable<string> toCopy)
    {
        Parallel.ForEach(toCopy, (dir, cancelToken) =>
        {
            string partialDir = dir[workDir.Length..];

            if (partialDir.StartsWith($"{Path.DirectorySeparatorChar}.{Path.DirectorySeparatorChar}"))
            {
                partialDir = partialDir[3..];
            }
            string directoryToCreate = $"{targetDir}{partialDir}";

            if (!Directory.Exists(directoryToCreate))
            {
                //Console.WriteLine($"Creating Dir {directoryToCreate}");
                Directory.CreateDirectory(directoryToCreate);
            }
            IEnumerable<string> files = Directory.EnumerateFiles(dir);

            Parallel.ForEach(files, (file, token) =>
            {
                string partialSourceFilePath = file[workDir.Length..];
                if (partialSourceFilePath.StartsWith($"{Path.DirectorySeparatorChar}.{Path.DirectorySeparatorChar}"))
                {
                    partialSourceFilePath = partialSourceFilePath[3..];
                }
                string targetFilePath = $"{targetDir}{partialSourceFilePath}";

                bool found = FileSystemHelper.Regex.RegexCache.Any(r => r.IsMatch(partialSourceFilePath));
                if (!found)
                {
                    File.Copy(
                        Path.GetFullPath($"{workDir}{Path.DirectorySeparatorChar}{partialSourceFilePath}"),
                        $"{targetFilePath}", true);
                }
            });
        });
    }
}
